﻿using DGN21.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DGN21.Controllers
{
    [Authorize(Roles = "ADMINISTRADOR")]
    public class AdminController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        readonly ApplicationDbContext db = new ApplicationDbContext();

        public AdminController()
        {
        }

        public AdminController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Usuarios()
        {
            return View();
        }

        public ActionResult NotFound()
        {
            ModelState.AddModelError(string.Empty, "No se encontró solicitud con ese folio, verifique el número en su correo de confirmación de la solicitud.");

            return View("Usuarios");
        }

        [HttpPost]
        public ActionResult Usuario(ApplicationUser model)
        {
            ViewBag.Roles = db.Roles.ToList();
            try
            {
                if (!ModelState.IsValid)
                {
                    throw new Exception("Correo Inválido.");
                }

                ApplicationUser user = UserManager.FindByEmail(model.Email);
                if (user.Roles.Count > 0)
                {
                    user.PhoneNumber = user.Roles.FirstOrDefault().RoleId;
                }
                return View(user);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            return RedirectToAction("NotFound", "Home");
        }

        [HttpPost]
        public ActionResult UsuarioRol(ApplicationUser model)
        {
            ViewBag.Roles = db.Roles.ToList();
            try
            {
                var idUsuario = model.Id;
                var rol = string.Empty;
                if (!string.IsNullOrEmpty(model.PhoneNumber))
                {
                    rol = db.Roles.Where(x=>x.Id == model.PhoneNumber).FirstOrDefault().Name;
                }

                ApplicationUser user = UserManager.FindById(idUsuario);
                if (user.Roles.Count > 0)
                {
                    var roleAnterior = user.Roles.FirstOrDefault().RoleId;
                    var roleNameAnterior = db.Roles.Where(x => x.Id == roleAnterior).FirstOrDefault().Name;
                    UserManager.RemoveFromRole(idUsuario, roleNameAnterior);
                }
                UserManager.AddToRole(idUsuario, rol);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return RedirectToAction("Usuario", model);
            }

            return RedirectToAction("Usuarios");
        }
    }
}
