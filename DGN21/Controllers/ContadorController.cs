﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DGN21.Controllers
{
    [Authorize(Roles = "CONTADOR(A)")]
    public class ContadorController : Controller
    {
        // GET: Contador
        public ActionResult Index()
        {
            return View();
        }
    }
}