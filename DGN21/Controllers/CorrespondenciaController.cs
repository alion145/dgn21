﻿using DGN21.Models;
using DGN21.Models.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DGN21.Controllers
{    
    public class CorrespondenciaController : Controller
    {
        readonly ApplicationDbContext db = new ApplicationDbContext();

        [Authorize(Roles = "RECEPCION,SECRETARIA(O)")]
        public ActionResult Nueva(int id = 0)
        {
            if(id.Equals(0))
            {
                return View(new CorrespondenciaModels());
            }
            else
            {
                var correspondecia = db.Correspondencia.Where(x => x.IdCorrespondencia == id).FirstOrDefault();
                return View(correspondecia);
            }
        }

        [Authorize(Roles = "RECEPCION,SECRETARIA(O)")]
        [HttpPost]
        public ActionResult Guardar(CorrespondenciaModels correspondencia)
        {
            try
            {
                if (ModelState.ContainsKey("IdCorrespondencia"))
                    ModelState["IdCorrespondencia"].Errors.Clear();
                if (!ModelState.IsValid)
                {
                    return View("Nueva", correspondencia);
                }

                if (correspondencia.IdCorrespondencia.Equals(0))
                {
                    correspondencia.IdUserCreacion = User.Identity.GetUserId();
                    db.Correspondencia.Add(correspondencia);
                }
                else
                {
                    db.Entry(correspondencia).State = EntityState.Modified;
                }

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
            
            return RedirectToAction("Nueva", new { id = correspondencia.IdCorrespondencia });
        }

        [Authorize(Roles = "RECEPCION,SECRETARIA(O)")]
        public ActionResult Inbox()
        {
            var correspondencia = db.Correspondencia.OrderByDescending(x=>x.IdCorrespondencia).ToList();
            return View(correspondencia);
        }

        [Authorize(Roles = "RECEPCION,DIRECTOR(A),SECRETARIA(O)")]
        public ActionResult Reporte()
        {
            var solicitudes = new List<SolicitudesModels>();
            var allSolicitudes = db.Solicitudes.ToList();
            CorrespondenciaViewModels correspondenciaViewModels = new CorrespondenciaViewModels();
            correspondenciaViewModels.CorrespondenciaInbox = db.Correspondencia.ToList();

            return View(correspondenciaViewModels);
        }

        [Authorize(Roles = "RECEPCION,DIRECTOR(A),SECRETARIA(O)")]
        [HttpPost]
        public ActionResult Reporte(FiltroReporteModels filtroReporte)
        {
            filtroReporte.FechaFinReporte = new DateTime(filtroReporte.FechaFinReporte.Value.Year, filtroReporte.FechaFinReporte.Value.Month, filtroReporte.FechaFinReporte.Value.Day, 23, 59, 59, 999);
            var correspondenciaInbox = new List<CorrespondenciaModels>();
            if (filtroReporte.FechaInicioReporte == null && filtroReporte.FechaFinReporte == null)
            {
                correspondenciaInbox = db.Correspondencia.ToList();
            }
            else if (filtroReporte.FechaInicioReporte != null && filtroReporte.FechaFinReporte != null)
            {
                correspondenciaInbox = db.Correspondencia.Where(x=> x.FechaCreacion >= filtroReporte.FechaInicioReporte && x.FechaCreacion <= filtroReporte.FechaFinReporte).ToList();
            }
            else if (filtroReporte.FechaInicioReporte != null && filtroReporte.FechaFinReporte == null)
            {
                correspondenciaInbox = db.Correspondencia.Where(x => x.FechaCreacion > filtroReporte.FechaInicioReporte).ToList();
            }
            else if (filtroReporte.FechaInicioReporte == null && filtroReporte.FechaFinReporte != null)
            {
                correspondenciaInbox = db.Correspondencia.Where(x => x.FechaCreacion < filtroReporte.FechaFinReporte).ToList();
            }

            CorrespondenciaViewModels correspondenciaViewModels = new CorrespondenciaViewModels();
            correspondenciaViewModels.FiltroReporte = filtroReporte;
            correspondenciaViewModels.CorrespondenciaInbox = correspondenciaInbox;

            return View("Reporte", correspondenciaViewModels);
        }
    }
}
