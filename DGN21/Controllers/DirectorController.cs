﻿using DGN21.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DGN21.Controllers
{
    [Authorize(Roles = "DIRECTOR(A)")]
    public class DirectorController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
