﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DGN21.Controllers
{
    [Authorize(Roles = "SECRETARIA(O)")]
    public class SecretariaController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
    }
}
