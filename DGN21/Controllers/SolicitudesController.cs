﻿using DGN21.Models;
using DGN21.Models.ViewModels;
using Microsoft.AspNet.Identity;
using reCAPTCHA.MVC;
using Rotativa;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace DGN21.Controllers
{
    public class SolicitudesController : Controller
    {
        readonly ApplicationDbContext db = new ApplicationDbContext();

        readonly string avisoPrivacidad = "<br/><br/><br/><strong><p>AVISO DE PRIVACIDAD SIMPLIFICADO</p><p>I. Denominación del Responsable:</p><p style='color: #833C0B;'>Dirección General de Notarías</p></strong><p>Los datos personales recabados por esta Dirección, dependiente de la Secretaría de Gobierno del Estado de Sonora, serán protegidos, incorporados y tratados de conformidad con lo previsto en la Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados y la Ley de Protección de Datos Personales en Posesión de Sujetos Obligados del Estado de Sonora y demás normatividad que resulte aplicable.</p>"
            + "<strong><p>II. La finalidad del tratamiento de sus datos personales es:</p></strong><p>Los datos personales otorgados a esta Dirección, serán utilizados única y exclusivamente para brindar la atención y respuesta a la solicitud de algún trámite y/o servicio, y serán resguardados por el personal responsable del trámite o servicio en cuestión. Todos los servicios y trámites que ofrece la Dirección serán entregados a las personas que intervengan o acrediten interés jurídico en la escritura que obre en los Archivos de la Dirección General de Notarías.</p>"
            + "<strong><p>III. De las Transferencias:</p></strong><p>Se hace de su conocimiento que sus datos personales podrán ser transmitidos a otras autoridades siempre y cuando los datos se utilicen para el ejercicio de facultades propias de las mismas, además de otras transmisiones previstas en la Ley.</p>"
            + "<strong><p>IV. Mecanismos y medios disponibles para que el Titular de los datos personales pueda manifestar su negativa para el tratamiento de sus datos personales.</p></strong><p>La Secretaría de Gobierno a través de las unidades administrativas de su adscripción ante las cuales se proporcionen datos personales, pondrá a consideración del ciudadano, a través de un manifiesto expreso, la autorización o no de la transferencia de sus datos personales a otras autoridades, cuyo tratamiento sea susceptible de transferencia. Conforme al Art. 120 fracción VIII de la Ley del Notariado para el Estado de Sonora.</p>"
            + "<strong><p>V. El aviso de privacidad integral podrá ser consultado en las siguientes páginas electrónicas:</p></strong><p><a href='https://notarias.sonora.gob.mx/'>notarias.sonora.gob.mx</a><br/><a href='https://notarias.sonora.gob.mx/conocenos/aviso-de-privacidad.html'>notarias.sonora.gob.mx/conocenos/aviso-de-privacidad.html</a></p>";

        public enum TipoTramite : int { CopiaSimple = 1, CopiaCertificada = 2, Testimonio = 3, DispTestamentaria = 4, BusquedaAnio = 5, TerminacionEsc = 6, Otros = 7 };

        // GET: Solicitudes
        public ActionResult Solicitud()
        {
            ViewBag.TiposTramite = db.TiposTramites.ToList();

            return View(new SolicitudesModels());
        }

        public ActionResult Folio(int id = 0)
        {
            if (!id.Equals(0))
            {
                SolicitudesModels solicitud = db.Solicitudes.Where(x => x.IdSolicitud == id).FirstOrDefault();
                solicitud.Documentos = db.Documentos.Where(x => x.IdSolicitud == id).ToList();
                return View(solicitud);
            }

            return View(new SolicitudesModels());
        }

        public bool EnviarCorreoSolicitud(int idSolicitud)
        {
            var envio = false;
            SolicitudesModels solicitud = db.Solicitudes.Where(x => x.IdSolicitud == idSolicitud).FirstOrDefault();

            var body = "<p>Estimado(a) Ciudadano(a) {0} {1}</p><p>Su solicitud se registró con éxito, con el folio siguiente: {2}</p><br/><p>Para seguimiento de su solicitud ingrese con su folio a <a href='http://tramitesdgn.sonora.gob.mx/'>tramitesdgn.sonora.gob.mx</a></p>";
            body += avisoPrivacidad;
            var message = new MailMessage();
            message.To.Add(new MailAddress(solicitud.CorreoElectronico));  // replace with valid value 
            message.From = new MailAddress("dgnsonora@sonora.gob.mx", "DIRECCION GENERAL DE NOTARIAS");  // replace with valid value
            message.Subject = "Confirmación de solicitud";
            message.Body = string.Format(body, solicitud.NombreSolicitante, solicitud.ApellidoPaterno, solicitud.IdSolicitud);
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "dgnsonora@sonora.gob.mx",  // replace with valid value
                    Password = "NOTARIASSON2020*"  // replace with valid value
                };
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = credential;
                smtp.Port = 587;
                smtp.Send(message);
                envio = true;
            }

            return envio;
        }

        [HttpPost]
        public ActionResult Guardar(SolicitudesModels solicitudesModels)
        {
            try
            {
                if (ModelState.ContainsKey("Solicitud.IdSolicitud"))
                    ModelState["Solicitud.IdSolicitud"].Errors.Clear();
                if (solicitudesModels.TipoTramite == (int)TipoTramite.DispTestamentaria)
                {
                    if (string.IsNullOrEmpty(solicitudesModels.Cujus))
                    {
                        ModelState.AddModelError("Cujus", "Debe llenar la información de nombre de Cujos (finado).");
                    }
                    if (string.IsNullOrEmpty(solicitudesModels.Juzgado))
                    {
                        ModelState.AddModelError("Juzgado", "Debe llenar la información de juzgado ó notaría que lo solicita.");
                    }
                }

                if (!ModelState.IsValid)
                {
                    ViewBag.TiposTramite = db.TiposTramites.ToList();
                    return View("Solicitud", solicitudesModels);
                }

                solicitudesModels.EstatusSolicitud = db.EstatusSolicitudes.Where(x => x.EstatusSolicitud.Contains("En revisión")).FirstOrDefault().IdEstatusSolicitud;
                solicitudesModels.FechaActualizacionEstatus = DateTime.Now;
                if (solicitudesModels.IdSolicitud.Equals(0))
                {
                    solicitudesModels.IdUserCreacion = User.Identity.GetUserId();
                    db.Solicitudes.Add(solicitudesModels);
                }
                else
                {
                    db.Entry(solicitudesModels).State = EntityState.Modified;
                }

                db.SaveChanges();
                if (!this.EnviarCorreoSolicitud(solicitudesModels.IdSolicitud))
                {
                    throw new Exception("No se pudo enviar correo de confirmación de solicitud.");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            return RedirectToAction("Folio", new { id = solicitudesModels.IdSolicitud });
        }

        [HttpPost]
        public ActionResult SubirArchivo(HttpPostedFileBase file, int id)
        {
            if (file != null && file.ContentLength > 0)
            {
                // Extraemos el contenido en Bytes del archivo subido.
                var _contenido = new byte[file.ContentLength];
                file.InputStream.Read(_contenido, 0, file.ContentLength);

                // Separamos el Nombre del archivo de la Extensión.
                int indiceDelUltimoPunto = file.FileName.LastIndexOf('.');
                string _nombre = file.FileName.Substring(0, indiceDelUltimoPunto);
                string _extension = file.FileName.Substring(indiceDelUltimoPunto + 1,
                                    file.FileName.Length - indiceDelUltimoPunto - 1);

                // Instanciamos la clase Archivo y asignammos los valores.
                DocumentosModels _documento = new DocumentosModels()
                {
                    Nombre = _nombre,
                    Extension = _extension,
                    IdSolicitud = id
                };

                try
                {
                    db.Documentos.Add(_documento);
                    db.SaveChanges();
                    // Subimos el archivo al Servidor.
                    _documento.SubirArchivo(_contenido);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }

            return RedirectToAction("Folio", new { id = id });
        }

        [HttpGet]
        public ActionResult DescargarArchivo(int id)
        {
            DocumentosModels _documento;
            FileContentResult _fileContent;

            using (db)
            {
                _documento = db.Documentos.FirstOrDefault(x => x.IdDocumento == id);
            }

            if (_documento == null)
            {
                return HttpNotFound();
            }
            else
            {
                try
                {
                    // Descargamos el archivo del Servidor.
                    _fileContent = new FileContentResult(_documento.DescargarArchivo(),
                                                         "application/octet-stream");
                    _fileContent.FileDownloadName = _documento.Nombre + "." + _documento.Extension;

                    return _fileContent;
                }
                catch (Exception)
                {
                    return HttpNotFound();
                }
            }
        }

        [HttpGet]
        public ActionResult EliminarArchivo(int id)
        {
            DocumentosModels _documento = db.Documentos.FirstOrDefault(x => x.IdDocumento == id);

            if (_documento != null)
            {
                int idSolicitud = _documento.IdSolicitud;
                using (db)
                {
                    _documento = db.Documentos.FirstOrDefault(x => x.IdDocumento == id);
                    db.Documentos.Remove(_documento);
                    if (db.SaveChanges() > 0)
                    {
                        // Eliminamos el archivo del Servidor.
                        _documento.EliminarArchivo();
                    }
                }

                return RedirectToAction("Folio", new { id = idSolicitud });
            }
            else
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        [CaptchaValidator(ErrorMessage = "Validación Captcha incorrecta.", RequiredMessage = "La validación Captcha es requerida.")]
        public ActionResult Consulta(SolicitudesModels solicitud, bool captchaValid)
        {
            ViewBag.TiposTramite = db.TiposTramites.ToList();
            ViewBag.EstatusSolicitudes = db.EstatusSolicitudes.ToList();
            try
            {
                if (!captchaValid)
                {
                    throw new Exception("Validación Captcha incorrecta, intente de nuevo.");
                }
                if (ModelState.ContainsKey("NombreSolicitante"))
                    ModelState["NombreSolicitante"].Errors.Clear();
                if (ModelState.ContainsKey("ApellidoPaterno"))
                    ModelState["ApellidoPaterno"].Errors.Clear();
                if (ModelState.ContainsKey("CorreoElectronico"))
                    ModelState["CorreoElectronico"].Errors.Clear();
                if (ModelState.ContainsKey("Telefono"))
                    ModelState["Telefono"].Errors.Clear();
                if (!solicitud.IdSolicitud.Equals(0))
                {
                    SolicitudesModels _solicitud = db.Solicitudes.Where(x => x.IdSolicitud == solicitud.IdSolicitud && x.CorreoElectronico == solicitud.CorreoElectronico).FirstOrDefault();
                    if (_solicitud == null)
                    {
                        throw new Exception("Su folio de solicitud debe coincidir con su correo electrónico de seguimiento.");
                    }

                    SolicitudesViewModels solicitudesViewModels = new SolicitudesViewModels
                    {
                        Solicitud = _solicitud,
                        SolicitudBusqueda = db.SolicitudesBusqueda.Where(x => x.IdSolicitud == solicitud.IdSolicitud).FirstOrDefault(),
                        PapeletasPago = db.PapeletasPago.Where(x => x.IdSolicitud == solicitud.IdSolicitud).FirstOrDefault(),
                        Observaciones = db.ObservacionesSolicitudes.Where(x => x.IdSolicitud == solicitud.IdSolicitud).ToList()
                    };

                    return View(solicitudesViewModels);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return RedirectToAction("NotFound", "Home", new { msj = ex.Message });
            }

            return RedirectToAction("NotFound", "Home", new { msj = "No se pudo realizar la consulta de solicitud. Ingrese todos los datos." });
        }

        [Authorize(Roles = "ADMINISTRADOR,SECRETARIA(O),DIRECTOR(A)")]
        public ActionResult InboxTurnar()
        {
            var solicitudes = new List<SolicitudesModels>();
            var allSolicitudes = db.Solicitudes.ToList();
            allSolicitudes.ForEach(p =>
            {
                var turnados = db.Turnados.ToList().Where(x => x.IdSolicitud == p.IdSolicitud);
                if (!turnados.Any())
                {
                    solicitudes.Add(p);
                }
            });
            return View(solicitudes);
        }

        [Authorize(Roles = "ADMINISTRADOR,SECRETARIA(O),DIRECTOR(A)")]
        public ActionResult InboxAsignada()
        {
            var solicitudes = new List<SolicitudesModels>();
            var allSolicitudes = db.Solicitudes.ToList();
            allSolicitudes.ForEach(p =>
            {
                var turnados = db.Turnados.ToList().Where(x => x.IdSolicitud == p.IdSolicitud);
                if (turnados.Any())
                {
                    solicitudes.Add(p);
                }
            });
            return View(solicitudes);
        }

        [Authorize(Roles = "ADMINISTRADOR,RECEPCION,SECRETARIA(O),DIRECTOR(A)")]
        public ActionResult Detalle(int id = 0)
        {
            ViewBag.TiposTramite = db.TiposTramites.ToList();
            ViewBag.UsersTurnar = db.Users.ToList();
            try
            {
                if (!id.Equals(0))
                {
                    SolicitudesModels _solicitud = db.Solicitudes.Where(x => x.IdSolicitud == id).FirstOrDefault();
                    if (_solicitud != null)
                    {
                        _solicitud.Documentos = db.Documentos.Where(x => x.IdSolicitud == id).ToList();
                        List<TurnadosModels> turnados = db.Turnados.Where(x => x.IdSolicitud == id).ToList();
                        if (turnados.Count > 0)
                        {
                            _solicitud.IdUserCreacion = turnados.LastOrDefault().IdUserTurnado;
                        }
                    }
                    else
                    {
                        throw new Exception("No se pudo realizar la consulta de solicitud.");
                    }

                    return View(_solicitud);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            return RedirectToAction("InboxTurnar");
        }

        [Authorize(Roles = "ADMINISTRADOR,RECEPCION,SECRETARIA(O),DIRECTOR(A)")]
        [HttpPost]
        public ActionResult Turnar(SolicitudesModels solicitudesModels)
        {
            try
            {
                if (!solicitudesModels.IdSolicitud.Equals(0) && !string.IsNullOrEmpty(solicitudesModels.IdUserCreacion))
                {
                    var turnar = new TurnadosModels()
                    {
                        IdSolicitud = solicitudesModels.IdSolicitud,
                        IdUserTurnado = solicitudesModels.IdUserCreacion,
                        IdUserCreacion = User.Identity.GetUserId()
                    };
                    db.Turnados.Add(turnar);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            return RedirectToAction("Detalle", new { id = solicitudesModels.IdSolicitud });
        }

        [Authorize(Roles = "ABOGADO(A),ARCHIVO")]
        public ActionResult InboxTurnadas()
        {
            var solicitudes = new List<SolicitudesModels>();
            var allSolicitudes = db.Solicitudes.ToList();
            allSolicitudes.ForEach(p =>
            {
                var turnados = db.Turnados.ToList().Where(x => x.IdSolicitud == p.IdSolicitud && x.IdUserTurnado == User.Identity.GetUserId());
                if (turnados.Any())
                {
                    solicitudes.Add(p);
                }
            });
            return View(solicitudes);
        }

        [Authorize(Roles = "ABOGADO(A)")]
        public ActionResult Turnada(int id = 0)
        {
            SolicitudesViewModels solicitud = new SolicitudesViewModels();
            ViewBag.TiposTramite = db.TiposTramites.ToList();
            ViewBag.EstatusSolicitudes = db.EstatusSolicitudes.ToList();
            ViewBag.FormatosPapeletas = db.FormatosPapeletas.ToList();
            try
            {
                if (!id.Equals(0))
                {
                    SolicitudesModels _solicitud = db.Solicitudes.Where(x => x.IdSolicitud == id).FirstOrDefault();
                    if (_solicitud != null)
                    {
                        _solicitud.Documentos = db.Documentos.Where(x => x.IdSolicitud == id).ToList();
                        ViewBag.FormatosPapeletas = db.FormatosPapeletas.Where(x => x.IdTipoTramite == _solicitud.TipoTramite).ToList();
                    }
                    else
                    {
                        throw new Exception("No se pudo realizar la consulta de solicitud.");
                    }

                    solicitud.Solicitud = _solicitud;
                    solicitud.SolicitudBusqueda = db.SolicitudesBusqueda.Where(x => x.IdSolicitud == id).FirstOrDefault();
                    solicitud.Turnada = db.Turnados.Where(x => x.IdSolicitud == id).OrderByDescending(o => o.IdTurnado).FirstOrDefault();
                    solicitud.PapeletasPago = db.PapeletasPago.Where(x => x.IdSolicitud == id).FirstOrDefault();
                    solicitud.Observaciones = db.ObservacionesSolicitudes.Where(x => x.IdSolicitud == id).ToList();
                    return View(solicitud);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            return RedirectToAction("InboxTurnadas");
        }

        [Authorize(Roles = "ABOGADO(A)")]
        public bool EnviarCorreoEstatusSolicitud(int idSolicitud)
        {
            var envio = false;
            SolicitudesModels solicitud = db.Solicitudes.Where(x => x.IdSolicitud == idSolicitud).FirstOrDefault();

            var body = "<p>Estimado(a) Ciudadano(a) {0} {1}</p><p>El estado de su solicitud ha sido actualizado, con el folio siguiente: {2}</p><br/><p>Para seguimiento de su solicitud ingrese con su folio a <a href='http://tramitesdgn.sonora.gob.mx/'>tramitesdgn.sonora.gob.mx</a></p>";
            body += avisoPrivacidad;
            var message = new MailMessage();
            message.To.Add(new MailAddress(solicitud.CorreoElectronico));  // replace with valid value 
            message.From = new MailAddress("dgnsonora@sonora.gob.mx", "DIRECCION GENERAL DE NOTARIAS");  // replace with valid value
            message.Subject = "Estatus de solicitud";
            message.Body = string.Format(body, solicitud.NombreSolicitante, solicitud.ApellidoPaterno, solicitud.IdSolicitud);
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "dgnsonora@sonora.gob.mx",  // replace with valid value
                    Password = "NOTARIASSON2020*"  // replace with valid value
                };
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = credential;
                smtp.Port = 587;
                smtp.Send(message);
                envio = true;
            }

            return envio;
        }

        [Authorize(Roles = "ABOGADO(A)")]
        [HttpPost]
        public ActionResult ActualizarEstatus(SolicitudesViewModels solicitudesModels)
        {
            try
            {
                if (!solicitudesModels.Solicitud.IdSolicitud.Equals(0) && !solicitudesModels.Solicitud.EstatusSolicitud.Equals(0))
                {
                    var solicitud = db.Solicitudes.Where(x => x.IdSolicitud == solicitudesModels.Solicitud.IdSolicitud).FirstOrDefault();
                    solicitud.EstatusSolicitud = solicitudesModels.Solicitud.EstatusSolicitud;
                    solicitud.FechaActualizacionEstatus = DateTime.Now;
                    db.Entry(solicitud).State = EntityState.Modified;
                    db.SaveChanges();

                    if (solicitudesModels.Observacion != null && !string.IsNullOrEmpty(solicitudesModels.Observacion.Observacion))
                    {
                        ApplicationDbContext db2 = new ApplicationDbContext();
                        ObservacionesSolicitudesModels observacionesSolicitudes = new ObservacionesSolicitudesModels
                        {
                            IdSolicitud = solicitudesModels.Solicitud.IdSolicitud,
                            Observacion = solicitudesModels.Observacion.Observacion.Trim(),
                            IdUserCreacion = User.Identity.GetUserId()
                        };
                        db2.ObservacionesSolicitudes.Add(observacionesSolicitudes);
                        db2.SaveChanges();

                    }

                    if (solicitud.EstatusSolicitudes.EstatusSolicitud.Equals("Pase a caja"))
                    {
                        PapeletasPagoModels papeletaPago = db.PapeletasPago.Where(x => x.IdSolicitud == solicitudesModels.Solicitud.IdSolicitud).FirstOrDefault();
                        if (!papeletaPago.IdPapeletaPago.Equals(0))
                        {
                            if (!this.EnviarCorreoPapeletaPago(solicitudesModels.Solicitud.IdSolicitud))
                            {
                                throw new Exception("No se pudo enviar correo de actualización de estatus de solicitud.");
                            }
                        }
                    }

                    if (!this.EnviarCorreoEstatusSolicitud(solicitudesModels.Solicitud.IdSolicitud))
                    {
                        throw new Exception("No se pudo enviar correo de actualización de estatus de solicitud.");
                    }
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            return RedirectToAction("Turnada", new { id = solicitudesModels.Solicitud.IdSolicitud });
        }

        [Authorize(Roles = "ABOGADO(A),ADMINISTRADOR,RECEPCION,SECRETARIA(O)")]
        public bool EnviarCorreoPapeletaPago(int idSolicitud)
        {
            var envio = false;
            SolicitudesModels solicitud = db.Solicitudes.Where(x => x.IdSolicitud == idSolicitud).FirstOrDefault();

            var body = "<p>Estimado(a) Ciudadano(a) {0} {1}</p><p>La papeleta de pago de su solicitud ya está lista para descargarse en Estatus de Solcitud, con el folio siguiente: {2}</p><br/><p>Para seguimiento de su solicitud ingrese con su folio a <a href='http://tramitesdgn.sonora.gob.mx/'>tramitesdgn.sonora.gob.mx</a></p>";
            body += avisoPrivacidad;
            var message = new MailMessage();
            message.To.Add(new MailAddress(solicitud.CorreoElectronico));  // replace with valid value 
            message.From = new MailAddress("dgnsonora@sonora.gob.mx", "DIRECCION GENERAL DE NOTARIAS");  // replace with valid value
            message.Subject = "Papeleta de pago de solicitud";
            message.Body = string.Format(body, solicitud.NombreSolicitante, solicitud.ApellidoPaterno, solicitud.IdSolicitud);
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "dgnsonora@sonora.gob.mx",  // replace with valid value
                    Password = "NOTARIASSON2020*"  // replace with valid value
                };
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = credential;
                smtp.Port = 587;
                smtp.Send(message);
                envio = true;
            }

            return envio;
        }

        [Authorize(Roles = "ABOGADO(A),ADMINISTRADOR,RECEPCION,SECRETARIA(O)")]
        [HttpPost]
        public ActionResult PapeletaPago(SolicitudesViewModels solicitudesModels)
        {
            try
            {
                if (ModelState.ContainsKey("PapeletasPago.IdPapeletaPago"))
                    ModelState["PapeletasPago.IdPapeletaPago"].Errors.Clear();
                if (ModelState.ContainsKey("PapeletasPago.IdFormatoPapeleta"))
                    ModelState["PapeletasPago.IdFormatoPapeleta"].Errors.Clear();
                if (ModelState.ContainsKey("PapeletasPago.Atendio"))
                    ModelState["PapeletasPago.Atendio"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.NombreSolicitante"))
                    ModelState["Solicitud.NombreSolicitante"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.ApellidoPaterno"))
                    ModelState["Solicitud.ApellidoPaterno"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.ApellidoMaterno"))
                    ModelState["Solicitud.ApellidoMaterno"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.CorreoElectronico"))
                    ModelState["Solicitud.CorreoElectronico"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.Telefono"))
                    ModelState["Solicitud.Telefono"].Errors.Clear();
                if (!ModelState.IsValid)
                {
                    throw new Exception("No se pudo guardar información de papeleta de pago. Debes llenar todos los campos.");
                }

                if (!solicitudesModels.Solicitud.IdSolicitud.Equals(0))
                {
                    solicitudesModels.PapeletasPago.IdSolicitud = solicitudesModels.Solicitud.IdSolicitud;
                    if (solicitudesModels.PapeletasPago.IdPapeletaPago.Equals(0))
                    {
                        db.PapeletasPago.Add(solicitudesModels.PapeletasPago);
                    }
                    else
                    {
                        db.Entry(solicitudesModels.PapeletasPago).State = EntityState.Modified;
                    }

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            return RedirectToAction("Turnada", new { id = solicitudesModels.Solicitud.IdSolicitud });
        }

        public ActionResult DownloadPapeletaPago(int id = 0)
        {
            PapeletasPagoModels papeletasPago = db.PapeletasPago.Where(x => x.IdSolicitud == id).FirstOrDefault();
            if (papeletasPago != null)
            {
                SolicitudesModels solicitud = db.Solicitudes.Where(x => x.IdSolicitud == id).FirstOrDefault();
                FormatosPapeletasModels formatosPapeletas = db.FormatosPapeletas.Where(x => x.IdFormatoPapeleta == papeletasPago.IdFormatoPapeleta).FirstOrDefault();
                SolicitudesViewModels solicitudesViewModels = new SolicitudesViewModels
                {
                    PapeletasPago = papeletasPago,
                    SolicitudBusqueda = db.SolicitudesBusqueda.Where(x => x.IdSolicitud == id).FirstOrDefault(),
                    Solicitud = solicitud
                };

                string nombreFormatoPapeleta = string.Empty;
                if(formatosPapeletas == null)
                {
                    nombreFormatoPapeleta = "PapeletaPagoDefault";
                }
                else
                {
                    switch (solicitud.TipoTramite)
                    {
                        case (int)TipoTramite.CopiaSimple:
                            nombreFormatoPapeleta = "PapeletaPago";
                            break;
                        case (int)TipoTramite.CopiaCertificada:
                            nombreFormatoPapeleta = "PapeletaPago2";
                            break;
                        case (int)TipoTramite.Testimonio:
                            nombreFormatoPapeleta = "PapeletaPago3";
                            break;
                        case (int)TipoTramite.DispTestamentaria:
                            nombreFormatoPapeleta = "PapeletaPago4";
                            break;
                        case (int)TipoTramite.BusquedaAnio:
                            nombreFormatoPapeleta = "PapeletaPago5";
                            break;
                        case (int)TipoTramite.TerminacionEsc:
                            nombreFormatoPapeleta = "PapeletaPago6";
                            break;
                        case (int)TipoTramite.Otros:
                            nombreFormatoPapeleta = "PapeletaPagoDefault";
                            break;
                        default:
                            nombreFormatoPapeleta = "PapeletaPagoDefault";
                            break;
                    }
                }

                // Define la URL de la Cabecera 
                //string _headerUrl = Url.Action("HeaderPDF", "Home", null, "https");
                // Define la URL del Pie de página
                //string _footerUrl = Url.Action("FooterPDF", "Home", null, "https");
                return new ViewAsPdf(nombreFormatoPapeleta, solicitudesViewModels)
                {
                    //// Establece la Cabecera y el Pie de página
                    //CustomSwitches = "--header-html " + _headerUrl + " --header-spacing 0 "+
                    //"--footer-html " + _footerUrl + " --footer-spacing 0"
                    //,
                    PageSize = Rotativa.Options.Size.A4
                    ,
                    FileName = "Papeleta de pago.pdf" // SI QUEREMOS QUE EL ARCHIVO SE DESCARGUE DIRECTAMENTE
                    ,
                    PageMargins = new Rotativa.Options.Margins(10, 10, 10, 10)
                };

            }

            return HttpNotFound();
        }

        [Authorize(Roles = "ABOGADO(A),ARCHIVO")]
        public ActionResult ReporteTurnadas()
        {
            var solicitudes = new List<SolicitudesModels>();
            var allSolicitudes = db.Solicitudes.ToList();
            ViewBag.EstatusSolicitudes = db.EstatusSolicitudes.ToList();
            SolicitudesViewModels solicitudesViewModels = new SolicitudesViewModels();
            solicitudesViewModels.SolicitudesInbox = new List<SolicitudesViewModels.SolicitudInbox>();
            allSolicitudes.ForEach(p =>
            {
                var turnados = db.Turnados.ToList().Where(x => x.IdSolicitud == p.IdSolicitud && x.IdUserTurnado == User.Identity.GetUserId());
                if (turnados.Any())
                {
                    var papeletasPago = db.PapeletasPago.Where(x => x.IdSolicitud == p.IdSolicitud).FirstOrDefault();
                    var turnada = turnados.LastOrDefault();
                    var observaciones = new List<ObservacionesSolicitudesModels>();
                    if(turnada != null)
                    {
                        observaciones = db.ObservacionesSolicitudes.Where(x => x.IdSolicitud == p.IdSolicitud && x.IdUserCreacion == turnada.IdUserTurnado).ToList();
                    }
                    SolicitudesViewModels.SolicitudInbox solicitudInbox = new SolicitudesViewModels.SolicitudInbox
                    {
                        Solicitud = p,
                        PapeletasPago = papeletasPago,
                        Turnada = turnada,
                        ObservacionesUltimoTurnado = observaciones
                    };
                    solicitudesViewModels.SolicitudesInbox.Add(solicitudInbox);
                }
            });
            
            return View(solicitudesViewModels);
        }

        [Authorize(Roles = "ABOGADO(A),ARCHIVO")]
        [HttpPost]
        public ActionResult ReporteTurnadas(FiltroReporteModels filtroReporte)
        {
            var solicitudes = new List<SolicitudesModels>();
            var allSolicitudes = new List<SolicitudesModels>();
            filtroReporte.FechaFinReporte = new DateTime(filtroReporte.FechaFinReporte.Value.Year, filtroReporte.FechaFinReporte.Value.Month, filtroReporte.FechaFinReporte.Value.Day, 23, 59, 59, 999);
            if (filtroReporte.FechaInicioReporte == null && filtroReporte.FechaFinReporte == null && filtroReporte.EstatusSolicitud.Equals(0))
            {
                allSolicitudes = db.Solicitudes.ToList();
            }
            else if (filtroReporte.EstatusSolicitud > 0 && filtroReporte.FechaInicioReporte != null && filtroReporte.FechaFinReporte != null)
            {
                allSolicitudes = db.Solicitudes.Where(x => x.EstatusSolicitud == filtroReporte.EstatusSolicitud && x.FechaCreacion >= filtroReporte.FechaInicioReporte && x.FechaCreacion <= filtroReporte.FechaFinReporte).ToList();
            }
            else if (filtroReporte.EstatusSolicitud > 0 && filtroReporte.FechaInicioReporte == null && filtroReporte.FechaFinReporte == null)
            {
                allSolicitudes = db.Solicitudes.Where(x => x.EstatusSolicitud == filtroReporte.EstatusSolicitud).ToList();
            }
            else if (filtroReporte.EstatusSolicitud.Equals(0) && filtroReporte.FechaInicioReporte != null && filtroReporte.FechaFinReporte != null)
            {
                allSolicitudes = db.Solicitudes.Where(x => x.FechaCreacion > filtroReporte.FechaInicioReporte && x.FechaCreacion < filtroReporte.FechaFinReporte).ToList();
            }
            else if (filtroReporte.FechaInicioReporte != null && filtroReporte.FechaFinReporte == null)
            {
                allSolicitudes = db.Solicitudes.Where(x => x.FechaCreacion > filtroReporte.FechaInicioReporte).ToList();
            }
            else if (filtroReporte.FechaInicioReporte == null && filtroReporte.FechaFinReporte != null)
            {
                allSolicitudes = db.Solicitudes.Where(x => x.FechaCreacion < filtroReporte.FechaFinReporte).ToList();
            }

            ViewBag.EstatusSolicitudes = db.EstatusSolicitudes.ToList();
            SolicitudesViewModels solicitudesViewModels = new SolicitudesViewModels();
            solicitudesViewModels.FiltroReporte = filtroReporte;
            solicitudesViewModels.SolicitudesInbox = new List<SolicitudesViewModels.SolicitudInbox>();
            allSolicitudes.ForEach(p =>
            {
                var turnados = db.Turnados.ToList().Where(x => x.IdSolicitud == p.IdSolicitud && x.IdUserTurnado == User.Identity.GetUserId());
                if (turnados.Any())
                {
                    var papeletasPago = db.PapeletasPago.Where(x => x.IdSolicitud == p.IdSolicitud).FirstOrDefault();
                    var turnada = turnados.LastOrDefault();
                    var observaciones = new List<ObservacionesSolicitudesModels>();
                    if (turnada != null)
                    {
                        observaciones = db.ObservacionesSolicitudes.Where(x => x.IdSolicitud == p.IdSolicitud && x.IdUserCreacion == turnada.IdUserTurnado).ToList();
                    }
                    SolicitudesViewModels.SolicitudInbox solicitudInbox = new SolicitudesViewModels.SolicitudInbox
                    {
                        Solicitud = p,
                        PapeletasPago = papeletasPago,
                        Turnada = turnada,
                        ObservacionesUltimoTurnado = observaciones
                    };
                    solicitudesViewModels.SolicitudesInbox.Add(solicitudInbox);
                }
            });

            return View("ReporteTurnadas", solicitudesViewModels);
        }

        [Authorize(Roles = "ABOGADO(A)")]
        [HttpPost]
        public ActionResult BajaTurnada(SolicitudesViewModels solicitudesModels)
        {
            try
            {
                if (solicitudesModels != null && solicitudesModels.Turnada != null)
                    if (!solicitudesModels.Turnada.IdTurnado.Equals(0))
                    {
                        var turnada = db.Turnados.Where(x => x.IdTurnado == solicitudesModels.Turnada.IdTurnado).FirstOrDefault();
                        solicitudesModels.Turnada.IdSolicitud = turnada.IdSolicitud;
                        turnada.Denegada = solicitudesModels.Turnada.Denegada;
                        turnada.ObservacionInterna = solicitudesModels.Turnada.ObservacionInterna;
                        db.Entry(turnada).State = EntityState.Modified;
                        db.SaveChanges();
                    }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            return RedirectToAction("Turnada", new { id = solicitudesModels.Turnada.IdSolicitud });
        }

        [Authorize(Roles = "ADMINISTRADOR,RECEPCION,SECRETARIA(O),DIRECTOR(A)")]
        public ActionResult SolicitarBusqueda(int id = 0)
        {
            SolicitudesBusquedaModels solicitudBusqueda = db.SolicitudesBusqueda.Where(x => x.IdSolicitud == id).FirstOrDefault();
            if (solicitudBusqueda == null)
            {
                SolicitudesModels solicitud = db.Solicitudes.Where(x => x.IdSolicitud == id).FirstOrDefault();
                solicitudBusqueda = new SolicitudesBusquedaModels
                {
                    IdSolicitud = id,
                    Solicitudes = solicitud
                };
            }
            return View("SolicitudBusqueda", solicitudBusqueda);
        }

        [Authorize(Roles = "ADMINISTRADOR,RECEPCION,SECRETARIA(O)")]
        public bool EnviarCorreoSolicitudB(int idSolicitud)
        {
            var solicitud = db.Solicitudes.Where(x => x.IdSolicitud == idSolicitud).FirstOrDefault();
            var envio = false;
            var body = "<p>Estimado(a) Ciudadano(a) {0} {1}</p><p>Su solicitud de búsqueda se registró con éxito, su folio de solicitud es: {2}</p><br/><p>Para seguimiento de su solicitud ingrese con su folio a <a href='http://tramitesdgn.sonora.gob.mx/'>tramitesdgn.sonora.gob.mx</a></p>";
            body += avisoPrivacidad;
            var message = new MailMessage();
            message.To.Add(new MailAddress(solicitud.CorreoElectronico));  // replace with valid value 
            message.From = new MailAddress("dgnsonora@sonora.gob.mx", "DIRECCION GENERAL DE NOTARIAS");  // replace with valid value
            message.Subject = "Confirmación de solicitud de búsqueda";
            message.Body = string.Format(body, solicitud.NombreSolicitante, solicitud.ApellidoPaterno, solicitud.IdSolicitud);
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "dgnsonora@sonora.gob.mx",  // replace with valid value
                    Password = "NOTARIASSON2020*"  // replace with valid value
                };
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = credential;
                smtp.Port = 587;
                smtp.Send(message);
                envio = true;
            }

            return envio;
        }

        [Authorize(Roles = "ADMINISTRADOR,RECEPCION,SECRETARIA(O),DIRECTOR(A)")]
        [HttpPost]
        public ActionResult GuardarBusqueda(SolicitudesBusquedaModels solicitudBusqueda)
        {
            try
            {
                if (ModelState.ContainsKey("Solicitud.IdSolicitudBusqueda"))
                    ModelState["Solicitud.IdSolicitudBusqueda"].Errors.Clear();
                if (!ModelState.IsValid)
                {
                    return View("SolicitarBusqueda", solicitudBusqueda);
                }

                solicitudBusqueda.IdUserCreacion = User.Identity.GetUserId();
                if (solicitudBusqueda.IdSolicitudBusqueda.Equals(0))
                {
                    db.SolicitudesBusqueda.Add(solicitudBusqueda);
                }
                else
                {
                    db.Entry(solicitudBusqueda).State = EntityState.Modified;
                }

                db.SaveChanges();
                if (!this.EnviarCorreoSolicitudB(solicitudBusqueda.IdSolicitud))
                {
                    throw new Exception("No se pudo enviar correo de confirmación de solicitud de búsqueda.");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            return RedirectToAction("Detalle", new { id = solicitudBusqueda.IdSolicitud });
        }

        [Authorize(Roles = "ARCHIVO")]
        public ActionResult TurnadaArchivo(int id = 0)
        {
            SolicitudesViewModels solicitud = new SolicitudesViewModels();
            ViewBag.TiposTramite = db.TiposTramites.ToList();
            ViewBag.EstatusSolicitudes = db.EstatusSolicitudes.ToList();
            ViewBag.UsersTurnar = db.Users.ToList();
            try
            {
                if (!id.Equals(0))
                {
                    SolicitudesModels _solicitud = db.Solicitudes.Where(x => x.IdSolicitud == id).FirstOrDefault();
                    if (_solicitud != null)
                    {
                        _solicitud.Documentos = db.Documentos.Where(x => x.IdSolicitud == id).ToList();
                    }
                    else
                    {
                        throw new Exception("No se pudo realizar la consulta de solicitud.");
                    }

                    solicitud.Solicitud = _solicitud;
                    solicitud.SolicitudBusqueda = db.SolicitudesBusqueda.Where(x => x.IdSolicitud == id).FirstOrDefault();
                    solicitud.Turnada = db.Turnados.Where(x => x.IdSolicitud == id).OrderByDescending(o => o.IdTurnado).FirstOrDefault();
                    return View(solicitud);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            return RedirectToAction("InboxTurnadas");
        }

        [Authorize(Roles = "ARCHIVO")]
        [HttpPost]
        public ActionResult GuardarArchivo(SolicitudesViewModels solicitudesViewModels)
        {
            try
            {
                if (ModelState.ContainsKey("Solicitud.Escritura"))
                    ModelState["Solicitud.Escritura"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.Vol"))
                    ModelState["Solicitud.Vol"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.NoNotaria"))
                    ModelState["Solicitud.NoNotaria"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.Notario"))
                    ModelState["Solicitud.Notario"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.Demarcacion"))
                    ModelState["Solicitud.Demarcacion"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.CompareceEscritura"))
                    ModelState["Solicitud.CompareceEscritura"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.NombreSolicitante"))
                    ModelState["Solicitud.NombreSolicitante"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.ApellidoPaterno"))
                    ModelState["Solicitud.ApellidoPaterno"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.ApellidoMaterno"))
                    ModelState["Solicitud.ApellidoMaterno"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.CorreoElectronico"))
                    ModelState["Solicitud.CorreoElectronico"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.Telefono"))
                    ModelState["Solicitud.Telefono"].Errors.Clear();
                if (!ModelState.IsValid)
                {
                    ViewBag.TiposTramite = db.TiposTramites.ToList();
                    ViewBag.EstatusSolicitudes = db.EstatusSolicitudes.ToList();
                    ViewBag.UsersTurnar = db.Users.ToList();
                    return RedirectToAction("TurnadaArchivo", new { id = solicitudesViewModels.Solicitud.IdSolicitud });
                }
                SolicitudesModels solicitud = db.Solicitudes.Where(x => x.IdSolicitud == solicitudesViewModels.Solicitud.IdSolicitud).FirstOrDefault();
                solicitud.Escritura = solicitudesViewModels.Solicitud.Escritura;
                solicitud.Vol = solicitudesViewModels.Solicitud.Vol;
                solicitud.Notario = solicitudesViewModels.Solicitud.Notario;
                solicitud.NoNotaria = solicitudesViewModels.Solicitud.NoNotaria;
                solicitud.Demarcacion = solicitudesViewModels.Solicitud.Demarcacion;
                solicitud.TipoTramite = solicitudesViewModels.Solicitud.TipoTramite;
                solicitud.CompareceEscritura = solicitudesViewModels.Solicitud.CompareceEscritura;
                db.Entry(solicitud).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            return RedirectToAction("TurnadaArchivo", new { id = solicitudesViewModels.Solicitud.IdSolicitud });
        }

        [Authorize(Roles = "ARCHIVO")]
        [HttpPost]
        public ActionResult TurnarArchivo(SolicitudesViewModels solicitudesViewModels)
        {
            try
            {
                if (!solicitudesViewModels.Turnada.IdSolicitud.Equals(0) && !string.IsNullOrEmpty(solicitudesViewModels.Turnada.IdUserTurnado))
                {
                    var idUserCreacion = User.Identity.GetUserId();
                    var turnar = db.Turnados.Where(x => x.IdSolicitud == solicitudesViewModels.Turnada.IdSolicitud && x.IdUserCreacion == idUserCreacion).FirstOrDefault();
                    if (turnar != null)
                    {
                        turnar.IdUserTurnado = solicitudesViewModels.Turnada.IdUserTurnado;
                        turnar.IdUserCreacion = idUserCreacion;
                        db.Entry(turnar).State = EntityState.Modified;
                    }
                    else
                    {
                        turnar = new TurnadosModels()
                        {
                            IdSolicitud = solicitudesViewModels.Turnada.IdSolicitud,
                            IdUserTurnado = solicitudesViewModels.Turnada.IdUserTurnado,
                            IdUserCreacion = idUserCreacion
                        };
                        db.Turnados.Add(turnar);
                    }

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            return RedirectToAction("TurnadaArchivo", new { id = solicitudesViewModels.Turnada.IdSolicitud });
        }

        [Authorize(Roles = "ARCHIVO")]
        [HttpPost]
        public ActionResult BajaTurnadaArchivo(SolicitudesViewModels solicitudesModels)
        {
            try
            {
                if (solicitudesModels != null && solicitudesModels.Turnada != null)
                    if (!solicitudesModels.Turnada.IdTurnado.Equals(0))
                    {
                        var turnada = db.Turnados.Where(x => x.IdTurnado == solicitudesModels.Turnada.IdTurnado).FirstOrDefault();
                        solicitudesModels.Turnada.IdSolicitud = turnada.IdSolicitud;
                        turnada.Denegada = solicitudesModels.Turnada.Denegada;
                        turnada.ObservacionInterna = solicitudesModels.Turnada.ObservacionInterna;
                        db.Entry(turnada).State = EntityState.Modified;
                        db.SaveChanges();
                    }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            return RedirectToAction("TurnadaArchivo", new { id = solicitudesModels.Turnada.IdSolicitud });
        }

        [Authorize(Roles = "ADMINISTRADOR,RECEPCION,SECRETARIA(O),DIRECTOR(A)")]
        public ActionResult PapeletaPagos(int id = 0)
        {
            SolicitudesViewModels solicitud = new SolicitudesViewModels();
            ViewBag.FormatosPapeletas = db.FormatosPapeletas.ToList();
            try
            {
                if (!id.Equals(0))
                {
                    SolicitudesModels _solicitud = db.Solicitudes.Where(x => x.IdSolicitud == id).FirstOrDefault();
                    if (_solicitud != null)
                    {
                        _solicitud.Documentos = db.Documentos.Where(x => x.IdSolicitud == id).ToList();
                        ViewBag.FormatosPapeletas = db.FormatosPapeletas.Where(x => x.IdTipoTramite == _solicitud.TipoTramite).ToList();
                    }
                    else
                    {
                        throw new Exception("No se pudo realizar la consulta de solicitud.");
                    }

                    solicitud.Solicitud = _solicitud;
                    solicitud.SolicitudBusqueda = db.SolicitudesBusqueda.Where(x => x.IdSolicitud == id).FirstOrDefault();
                    solicitud.PapeletasPago = db.PapeletasPago.Where(x => x.IdSolicitud == id).FirstOrDefault();
                    return View(solicitud);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            return RedirectToAction("InboxTurnar");
        }

        [Authorize(Roles = "ADMINISTRADOR,RECEPCION,SECRETARIA(O),DIRECTOR(A)")]
        [HttpPost]
        public ActionResult PapeletaPagos(SolicitudesViewModels solicitudesModels)
        {
            try
            {
                if (ModelState.ContainsKey("PapeletasPago.IdPapeletaPago"))
                    ModelState["PapeletasPago.IdPapeletaPago"].Errors.Clear();
                if (ModelState.ContainsKey("PapeletasPago.IdFormatoPapeleta"))
                    ModelState["PapeletasPago.IdFormatoPapeleta"].Errors.Clear();
                if (ModelState.ContainsKey("PapeletasPago.Cantidad"))
                    ModelState["PapeletasPago.Cantidad"].Errors.Clear();
                if (ModelState.ContainsKey("PapeletasPago.CantidadLetra"))
                    ModelState["PapeletasPago.CantidadLetra"].Errors.Clear();
                if (ModelState.ContainsKey("PapeletasPago.Atendio"))
                    ModelState["PapeletasPago.Atendio"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.NombreSolicitante"))
                    ModelState["Solicitud.NombreSolicitante"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.ApellidoPaterno"))
                    ModelState["Solicitud.ApellidoPaterno"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.ApellidoMaterno"))
                    ModelState["Solicitud.ApellidoMaterno"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.CorreoElectronico"))
                    ModelState["Solicitud.CorreoElectronico"].Errors.Clear();
                if (ModelState.ContainsKey("Solicitud.Telefono"))
                    ModelState["Solicitud.Telefono"].Errors.Clear();
                if (!ModelState.IsValid)
                {
                    throw new Exception("No se pudo guardar información de papeleta de pago. Debes llenar todos los campos.");
                }

                if (!solicitudesModels.Solicitud.IdSolicitud.Equals(0))
                {
                    solicitudesModels.PapeletasPago.IdSolicitud = solicitudesModels.Solicitud.IdSolicitud;
                    if (solicitudesModels.PapeletasPago.IdPapeletaPago.Equals(0))
                    {
                        db.PapeletasPago.Add(solicitudesModels.PapeletasPago);
                    }
                    else
                    {
                        db.Entry(solicitudesModels.PapeletasPago).State = EntityState.Modified;
                    }

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            return RedirectToAction("PapeletaPagos", new { id = solicitudesModels.Solicitud.IdSolicitud });
        }

        [Authorize(Roles = "ADMINISTRADOR,RECEPCION,SECRETARIA(O)")]
        public ActionResult ReporteSolicitudes()
        {
            var idUser = User.Identity.GetUserId();
            var solicitudes = new List<SolicitudesModels>();
            var allSolicitudes = db.Solicitudes.Where(x=>x.IdUserCreacion == idUser).ToList();
            ViewBag.EstatusSolicitudes = db.EstatusSolicitudes.ToList();
            SolicitudesViewModels solicitudesViewModels = new SolicitudesViewModels();
            solicitudesViewModels.SolicitudesInbox = new List<SolicitudesViewModels.SolicitudInbox>();
            allSolicitudes.ForEach(p =>
            {
                var papeletasPago = db.PapeletasPago.Where(x => x.IdSolicitud == p.IdSolicitud).FirstOrDefault();
                var turnada = db.Turnados.Where(x => x.IdSolicitud == p.IdSolicitud).OrderByDescending(o => o.IdTurnado).FirstOrDefault();
                SolicitudesViewModels.SolicitudInbox solicitudInbox = new SolicitudesViewModels.SolicitudInbox
                {
                    Solicitud = p,
                    PapeletasPago = papeletasPago,
                    Turnada = turnada
                };
                solicitudesViewModels.SolicitudesInbox.Add(solicitudInbox);
            });

            return View(solicitudesViewModels);
        }

        [Authorize(Roles = "ADMINISTRADOR,RECEPCION,SECRETARIA(O)")]
        [HttpPost]
        public ActionResult ReporteSolicitudes(FiltroReporteModels filtroReporte)
        {
            var idUser = User.Identity.GetUserId();
            var solicitudes = new List<SolicitudesModels>();
            var allSolicitudes = db.Solicitudes.Where(x => x.IdUserCreacion == idUser).ToList();
            filtroReporte.FechaFinReporte = new DateTime(filtroReporte.FechaFinReporte.Value.Year, filtroReporte.FechaFinReporte.Value.Month, filtroReporte.FechaFinReporte.Value.Day, 23, 59, 59, 999);
            if (filtroReporte.EstatusSolicitud > 0 && filtroReporte.FechaInicioReporte != null && filtroReporte.FechaFinReporte != null)
            {
                allSolicitudes = allSolicitudes.Where(x => x.EstatusSolicitud == filtroReporte.EstatusSolicitud && x.FechaCreacion >= filtroReporte.FechaInicioReporte && x.FechaCreacion <= filtroReporte.FechaFinReporte).ToList();
            }
            else if (filtroReporte.EstatusSolicitud > 0 && filtroReporte.FechaInicioReporte == null && filtroReporte.FechaFinReporte == null)
            {
                allSolicitudes = allSolicitudes.Where(x => x.EstatusSolicitud == filtroReporte.EstatusSolicitud).ToList();
            }
            else if (filtroReporte.EstatusSolicitud.Equals(0) && filtroReporte.FechaInicioReporte != null && filtroReporte.FechaFinReporte != null)
            {
                allSolicitudes = allSolicitudes.Where(x => x.FechaCreacion > filtroReporte.FechaInicioReporte && x.FechaCreacion < filtroReporte.FechaFinReporte).ToList();
            }
            else if (filtroReporte.FechaInicioReporte != null && filtroReporte.FechaFinReporte == null)
            {
                allSolicitudes = allSolicitudes.Where(x => x.FechaCreacion > filtroReporte.FechaInicioReporte).ToList();
            }
            else if (filtroReporte.FechaInicioReporte == null && filtroReporte.FechaFinReporte != null)
            {
                allSolicitudes = allSolicitudes.Where(x => x.FechaCreacion < filtroReporte.FechaFinReporte).ToList();
            }

            ViewBag.EstatusSolicitudes = db.EstatusSolicitudes.ToList();
            SolicitudesViewModels solicitudesViewModels = new SolicitudesViewModels();
            solicitudesViewModels.FiltroReporte = filtroReporte;
            solicitudesViewModels.SolicitudesInbox = new List<SolicitudesViewModels.SolicitudInbox>();
            allSolicitudes.ForEach(p =>
            {
                var turnada = db.Turnados.Where(x => x.IdSolicitud == p.IdSolicitud).OrderByDescending(o => o.IdTurnado).FirstOrDefault();
                var papeletasPago = db.PapeletasPago.Where(x => x.IdSolicitud == p.IdSolicitud).FirstOrDefault();
                SolicitudesViewModels.SolicitudInbox solicitudInbox = new SolicitudesViewModels.SolicitudInbox
                {
                    Solicitud = p,
                    PapeletasPago = papeletasPago,
                    Turnada = turnada
                };
                solicitudesViewModels.SolicitudesInbox.Add(solicitudInbox);
            });

            return View("ReporteSolicitudes", solicitudesViewModels);
        }

        [Authorize(Roles = "ADMINISTRADOR,DIRECTOR(A),SECRETARIA(O)")]
        public ActionResult ReporteAllSolicitudes()
        {
            var solicitudes = new List<SolicitudesModels>();
            var allSolicitudes = db.Solicitudes.ToList();
            ViewBag.EstatusSolicitudes = db.EstatusSolicitudes.ToList();
            SolicitudesViewModels solicitudesViewModels = new SolicitudesViewModels();
            solicitudesViewModels.SolicitudesInbox = new List<SolicitudesViewModels.SolicitudInbox>();
            allSolicitudes.ForEach(p =>
            {
                var papeletasPago = db.PapeletasPago.Where(x => x.IdSolicitud == p.IdSolicitud).FirstOrDefault();
                var turnados = db.Turnados.ToList().Where(x => x.IdSolicitud == p.IdSolicitud);
                var turnada = turnados.LastOrDefault();
                var observaciones = new List<ObservacionesSolicitudesModels>();
                if (turnada != null)
                {
                    observaciones = db.ObservacionesSolicitudes.Where(x => x.IdSolicitud == p.IdSolicitud && x.IdUserCreacion == turnada.IdUserTurnado).ToList();
                }
                SolicitudesViewModels.SolicitudInbox solicitudInbox = new SolicitudesViewModels.SolicitudInbox
                {
                    Solicitud = p,
                    PapeletasPago = papeletasPago,
                    Turnada = turnada,
                    ObservacionesUltimoTurnado = observaciones
                };
                solicitudesViewModels.SolicitudesInbox.Add(solicitudInbox);
            });

            return View(solicitudesViewModels);
        }

        [Authorize(Roles = "ADMINISTRADOR,DIRECTOR(A),SECRETARIA(O)")]
        [HttpPost]
        public ActionResult ReporteAllSolicitudes(FiltroReporteModels filtroReporte)
        {
            var solicitudes = new List<SolicitudesModels>();
            var allSolicitudes = new List<SolicitudesModels>();
            filtroReporte.FechaFinReporte = new DateTime(filtroReporte.FechaFinReporte.Value.Year, filtroReporte.FechaFinReporte.Value.Month, filtroReporte.FechaFinReporte.Value.Day, 23, 59, 59, 999);
            if (filtroReporte.FechaInicioReporte == null && filtroReporte.FechaFinReporte == null && filtroReporte.EstatusSolicitud.Equals(0))
            {
                allSolicitudes = db.Solicitudes.ToList();
            }
            else if (filtroReporte.EstatusSolicitud > 0 && filtroReporte.FechaInicioReporte != null && filtroReporte.FechaFinReporte != null)
            {
                allSolicitudes = db.Solicitudes.Where(x => x.EstatusSolicitud == filtroReporte.EstatusSolicitud && x.FechaCreacion >= filtroReporte.FechaInicioReporte && x.FechaCreacion <= filtroReporte.FechaFinReporte).ToList();
            }
            else if (filtroReporte.EstatusSolicitud > 0 && filtroReporte.FechaInicioReporte == null && filtroReporte.FechaFinReporte == null)
            {
                allSolicitudes = db.Solicitudes.Where(x => x.EstatusSolicitud == filtroReporte.EstatusSolicitud).ToList();
            }
            else if (filtroReporte.EstatusSolicitud.Equals(0) && filtroReporte.FechaInicioReporte != null && filtroReporte.FechaFinReporte != null)
            {
                allSolicitudes = db.Solicitudes.Where(x => x.FechaCreacion > filtroReporte.FechaInicioReporte && x.FechaCreacion < filtroReporte.FechaFinReporte).ToList();
            }
            else if (filtroReporte.FechaInicioReporte != null && filtroReporte.FechaFinReporte == null)
            {
                allSolicitudes = db.Solicitudes.Where(x => x.FechaCreacion > filtroReporte.FechaInicioReporte).ToList();
            }
            else if (filtroReporte.FechaInicioReporte == null && filtroReporte.FechaFinReporte != null)
            {
                allSolicitudes = db.Solicitudes.Where(x => x.FechaCreacion < filtroReporte.FechaFinReporte).ToList();
            }

            ViewBag.EstatusSolicitudes = db.EstatusSolicitudes.ToList();
            SolicitudesViewModels solicitudesViewModels = new SolicitudesViewModels();
            solicitudesViewModels.FiltroReporte = filtroReporte;
            solicitudesViewModels.SolicitudesInbox = new List<SolicitudesViewModels.SolicitudInbox>();
            allSolicitudes.ForEach(p =>
            {
                var papeletasPago = db.PapeletasPago.Where(x => x.IdSolicitud == p.IdSolicitud).FirstOrDefault();
                var turnados = db.Turnados.ToList().Where(x => x.IdSolicitud == p.IdSolicitud);
                var turnada = turnados.LastOrDefault();
                var observaciones = new List<ObservacionesSolicitudesModels>();
                if (turnada != null)
                {
                    observaciones = db.ObservacionesSolicitudes.Where(x => x.IdSolicitud == p.IdSolicitud && x.IdUserCreacion == turnada.IdUserTurnado).ToList();
                }
                SolicitudesViewModels.SolicitudInbox solicitudInbox = new SolicitudesViewModels.SolicitudInbox
                {
                    Solicitud = p,
                    PapeletasPago = papeletasPago,
                    Turnada = turnada,
                    ObservacionesUltimoTurnado = observaciones
                };
                solicitudesViewModels.SolicitudesInbox.Add(solicitudInbox);
            });

            return View("ReporteAllSolicitudes", solicitudesViewModels);
        }

        [Authorize(Roles = "CONTADOR(A)")]
        public ActionResult ReporteTurnadasUsuarios()
        {
            var solicitudes = new List<SolicitudesModels>();
            var allSolicitudes = db.Solicitudes.ToList();
            ViewBag.EstatusSolicitudes = db.EstatusSolicitudes.ToList();
            ViewBag.Usuarios = db.Users.ToList();
            SolicitudesViewModels solicitudesViewModels = new SolicitudesViewModels();
            solicitudesViewModels.SolicitudesInbox = new List<SolicitudesViewModels.SolicitudInbox>();
            allSolicitudes.ForEach(p =>
            {
                var papeletasPago = db.PapeletasPago.Where(x => x.IdSolicitud == p.IdSolicitud).FirstOrDefault();
                SolicitudesViewModels.SolicitudInbox solicitudInbox = new SolicitudesViewModels.SolicitudInbox
                {
                    Solicitud = p,
                    PapeletasPago = papeletasPago
                };
                solicitudesViewModels.SolicitudesInbox.Add(solicitudInbox);
            });
            
            return View(solicitudesViewModels);
        }

        [Authorize(Roles = "CONTADOR(A)")]
        [HttpPost]
        public ActionResult ReporteTurnadasUsuarios(FiltroReporteModels filtroReporte)
        {
            var solicitudes = new List<SolicitudesModels>();
            var allSolicitudes = new List<SolicitudesModels>();
            filtroReporte.FechaFinReporte = new DateTime(filtroReporte.FechaFinReporte.Value.Year, filtroReporte.FechaFinReporte.Value.Month, filtroReporte.FechaFinReporte.Value.Day, 23, 59, 59, 999);
            if (filtroReporte.FechaInicioReporte == null && filtroReporte.FechaFinReporte == null && filtroReporte.EstatusSolicitud.Equals(0))
            {
                allSolicitudes = db.Solicitudes.ToList();
            }
            else if (filtroReporte.EstatusSolicitud > 0 && filtroReporte.FechaInicioReporte != null && filtroReporte.FechaFinReporte != null)
            {
                allSolicitudes = db.Solicitudes.Where(x => x.EstatusSolicitud == filtroReporte.EstatusSolicitud && x.FechaCreacion >= filtroReporte.FechaInicioReporte && x.FechaCreacion <= filtroReporte.FechaFinReporte).ToList();
            }
            else if (filtroReporte.EstatusSolicitud > 0 && filtroReporte.FechaInicioReporte == null && filtroReporte.FechaFinReporte == null)
            {
                allSolicitudes = db.Solicitudes.Where(x => x.EstatusSolicitud == filtroReporte.EstatusSolicitud).ToList();
            }
            else if (filtroReporte.EstatusSolicitud.Equals(0) && filtroReporte.FechaInicioReporte != null && filtroReporte.FechaFinReporte != null)
            {
                allSolicitudes = db.Solicitudes.Where(x => x.FechaCreacion > filtroReporte.FechaInicioReporte && x.FechaCreacion < filtroReporte.FechaFinReporte).ToList();
            }
            else if (filtroReporte.FechaInicioReporte != null && filtroReporte.FechaFinReporte == null)
            {
                allSolicitudes = db.Solicitudes.Where(x => x.FechaCreacion > filtroReporte.FechaInicioReporte).ToList();
            }
            else if (filtroReporte.FechaInicioReporte == null && filtroReporte.FechaFinReporte != null)
            {
                allSolicitudes = db.Solicitudes.Where(x => x.FechaCreacion < filtroReporte.FechaFinReporte).ToList();
            }

            ViewBag.EstatusSolicitudes = db.EstatusSolicitudes.ToList();
            ViewBag.Usuarios = db.Users.ToList();
            SolicitudesViewModels solicitudesViewModels = new SolicitudesViewModels();
            solicitudesViewModels.FiltroReporte = filtroReporte;
            solicitudesViewModels.SolicitudesInbox = new List<SolicitudesViewModels.SolicitudInbox>();
            allSolicitudes.ForEach(p =>
            {
                if (string.IsNullOrEmpty(filtroReporte.IdUser))
                {
                    var papeletasPago = db.PapeletasPago.Where(x => x.IdSolicitud == p.IdSolicitud).FirstOrDefault();
                    SolicitudesViewModels.SolicitudInbox solicitudInbox = new SolicitudesViewModels.SolicitudInbox
                    {
                        Solicitud = p,
                        PapeletasPago = papeletasPago
                    };
                    solicitudesViewModels.SolicitudesInbox.Add(solicitudInbox);
                }
                else
                {
                    var turnados = db.Turnados.ToList().Where(x => x.IdSolicitud == p.IdSolicitud && x.IdUserTurnado == filtroReporte.IdUser);
                    if (turnados.Any())
                    {
                        var papeletasPago = db.PapeletasPago.Where(x => x.IdSolicitud == p.IdSolicitud).FirstOrDefault();
                        SolicitudesViewModels.SolicitudInbox solicitudInbox = new SolicitudesViewModels.SolicitudInbox
                        {
                            Solicitud = p,
                            PapeletasPago = papeletasPago
                        };
                        solicitudesViewModels.SolicitudesInbox.Add(solicitudInbox);
                    }
                }
            });

            return View("ReporteTurnadasUsuarios", solicitudesViewModels);
        }
    }
}