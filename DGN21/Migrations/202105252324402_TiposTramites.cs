namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TiposTramites : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TiposTramites",
                c => new
                    {
                        IdTipoTramite = c.Int(nullable: false, identity: true),
                        TipoTramite = c.String(nullable: false, maxLength: 50),
                        Activo = c.Boolean(nullable: false, defaultValue: true),
                    })
                .PrimaryKey(t => t.IdTipoTramite);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TiposTramites");
        }
    }
}
