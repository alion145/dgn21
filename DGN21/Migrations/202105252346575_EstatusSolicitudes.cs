namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EstatusSolicitudes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EstatusSolicitudes",
                c => new
                    {
                        IdEstatusSolicitud = c.Int(nullable: false, identity: true),
                        EstatusSolicitud = c.String(nullable: false, maxLength: 50),
                        Activo = c.Boolean(nullable: false, defaultValue: true),
                    })
                .PrimaryKey(t => t.IdEstatusSolicitud);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EstatusSolicitudes");
        }
    }
}
