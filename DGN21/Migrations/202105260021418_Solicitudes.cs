namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Solicitudes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Solicitudes",
                c => new
                    {
                        IdSolicitud = c.Int(nullable: false, identity: true),
                        NombreSolicitante = c.String(nullable: false, maxLength: 100),
                        ApellidoPaterno = c.String(nullable: false, maxLength: 100),
                        ApellidoMaterno = c.String(maxLength: 100),
                        CorreoElectronico = c.String(nullable: false, maxLength: 255),
                        Telefono = c.String(nullable: false, maxLength: 50),
                        Escritura = c.String(maxLength: 50),
                        Vol = c.String(maxLength: 50),
                        NoNotaria = c.String(maxLength: 50),
                        Notario = c.String(maxLength: 50),
                        Demarcacion = c.String(maxLength: 50),
                        CompareceEscritura = c.Boolean(nullable: false),
                        TipoTramite = c.Int(nullable: false),
                        EstatusSolicitud = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdSolicitud)
                .ForeignKey("dbo.EstatusSolicitudes", t => t.EstatusSolicitud, cascadeDelete: true)
                .ForeignKey("dbo.TiposTramites", t => t.TipoTramite, cascadeDelete: true)
                .Index(t => t.TipoTramite)
                .Index(t => t.EstatusSolicitud);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Solicitudes", "TipoTramite", "dbo.TiposTramites");
            DropForeignKey("dbo.Solicitudes", "EstatusSolicitud", "dbo.EstatusSolicitudes");
            DropIndex("dbo.Solicitudes", new[] { "EstatusSolicitud" });
            DropIndex("dbo.Solicitudes", new[] { "TipoTramite" });
            DropTable("dbo.Solicitudes");
        }
    }
}
