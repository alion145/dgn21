namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fechaSolicitud : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Solicitudes", "FechaCreacion", c => c.DateTime(nullable: false, defaultValueSql: "getdate()"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Solicitudes", "FechaCreacion");
        }
    }
}
