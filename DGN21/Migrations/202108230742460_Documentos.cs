namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Documentos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Documentos",
                c => new
                    {
                        IdDocumento = c.Int(nullable: false, identity: true),
                        IdSolicitud = c.Int(nullable: false),
                        FechaCreacion = c.DateTime(nullable: false, defaultValueSql: "getdate()"),
                        Nombre = c.String(nullable: false, maxLength: 100),
                        Extension = c.String(nullable: false, maxLength: 4),
                    })
                .PrimaryKey(t => t.IdDocumento)
                .ForeignKey("dbo.Solicitudes", t => t.IdSolicitud, cascadeDelete: true)
                .Index(t => t.IdSolicitud);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Documentos", "IdSolicitud", "dbo.Solicitudes");
            DropIndex("dbo.Documentos", new[] { "IdSolicitud" });
            DropTable("dbo.Documentos");
        }
    }
}
