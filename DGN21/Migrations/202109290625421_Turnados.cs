namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Turnados : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TurnadosSolicitudes",
                c => new
                    {
                        IdTurnado = c.Int(nullable: false, identity: true),
                        IdSolicitud = c.Int(nullable: false),
                        IdUserTurnado = c.String(nullable: false, maxLength: 128),
                        IdUserCreacion = c.String(nullable: false, maxLength: 128),
                        FechaCreacion = c.DateTime(nullable: false, defaultValueSql: "GetDate()"),
                    })
                .PrimaryKey(t => t.IdTurnado)
                .ForeignKey("dbo.AspNetUsers", t => t.IdUserCreacion)
                .ForeignKey("dbo.AspNetUsers", t => t.IdUserTurnado)
                .ForeignKey("dbo.Solicitudes", t => t.IdSolicitud, cascadeDelete: true)
                .Index(t => t.IdSolicitud)
                .Index(t => t.IdUserTurnado)
                .Index(t => t.IdUserCreacion);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TurnadosSolicitudes", "IdSolicitud", "dbo.Solicitudes");
            DropForeignKey("dbo.TurnadosSolicitudes", "IdUserTurnado", "dbo.AspNetUsers");
            DropForeignKey("dbo.TurnadosSolicitudes", "IdUserCreacion", "dbo.AspNetUsers");
            DropIndex("dbo.TurnadosSolicitudes", new[] { "IdUserCreacion" });
            DropIndex("dbo.TurnadosSolicitudes", new[] { "IdUserTurnado" });
            DropIndex("dbo.TurnadosSolicitudes", new[] { "IdSolicitud" });
            DropTable("dbo.TurnadosSolicitudes");
        }
    }
}
