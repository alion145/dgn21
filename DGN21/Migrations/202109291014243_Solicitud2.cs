namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Solicitud2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Solicitudes", "IdUserCreacion", c => c.String(maxLength: 128));
            CreateIndex("dbo.Solicitudes", "IdUserCreacion");
            AddForeignKey("dbo.Solicitudes", "IdUserCreacion", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Solicitudes", "IdUserCreacion", "dbo.AspNetUsers");
            DropIndex("dbo.Solicitudes", new[] { "IdUserCreacion" });
            DropColumn("dbo.Solicitudes", "IdUserCreacion");
        }
    }
}
