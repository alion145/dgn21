namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FormatosPapeletas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FormatosPapeletas",
                c => new
                    {
                        IdFormatoPapeleta = c.Int(nullable: false, identity: true),
                        IdTipoTramite = c.Int(nullable: false),
                        NombreFormato = c.String(nullable: false, maxLength: 100),
                        CveFiscal = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdFormatoPapeleta)
                .ForeignKey("dbo.TiposTramites", t => t.IdTipoTramite, cascadeDelete: true)
                .Index(t => t.IdTipoTramite);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FormatosPapeletas", "IdTipoTramite", "dbo.TiposTramites");
            DropIndex("dbo.FormatosPapeletas", new[] { "IdTipoTramite" });
            DropTable("dbo.FormatosPapeletas");
        }
    }
}
