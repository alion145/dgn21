namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FormatosPapeletas2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FormatosPapeletas", "CveFiscal", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FormatosPapeletas", "CveFiscal", c => c.Int(nullable: false));
        }
    }
}
