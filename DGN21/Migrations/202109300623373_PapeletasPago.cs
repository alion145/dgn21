namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PapeletasPago : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PapeletasPago",
                c => new
                    {
                        IdPapeletaPago = c.Int(nullable: false, identity: true),
                        IdSolicitud = c.Int(nullable: false),
                        Fecha = c.DateTime(),
                        Cantidad = c.Double(nullable: false),
                        CantidadLetra = c.String(maxLength: 255),
                        Atendio = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.IdPapeletaPago)
                .ForeignKey("dbo.Solicitudes", t => t.IdSolicitud, cascadeDelete: true)
                .Index(t => t.IdSolicitud);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PapeletasPago", "IdSolicitud", "dbo.Solicitudes");
            DropIndex("dbo.PapeletasPago", new[] { "IdSolicitud" });
            DropTable("dbo.PapeletasPago");
        }
    }
}
