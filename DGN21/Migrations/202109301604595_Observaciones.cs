namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Observaciones : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Observaciones",
                c => new
                    {
                        IdObservaciones = c.Int(nullable: false, identity: true),
                        IdSolicitud = c.Int(nullable: false),
                        Observacion = c.String(),
                        FechaCreacion = c.DateTime(nullable: false, defaultValueSql: "getdate()"),
                        IdUserCreacion = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.IdObservaciones)
                .ForeignKey("dbo.AspNetUsers", t => t.IdUserCreacion)
                .ForeignKey("dbo.Solicitudes", t => t.IdSolicitud, cascadeDelete: true)
                .Index(t => t.IdSolicitud)
                .Index(t => t.IdUserCreacion);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Observaciones", "IdSolicitud", "dbo.Solicitudes");
            DropForeignKey("dbo.Observaciones", "IdUserCreacion", "dbo.AspNetUsers");
            DropIndex("dbo.Observaciones", new[] { "IdUserCreacion" });
            DropIndex("dbo.Observaciones", new[] { "IdSolicitud" });
            DropTable("dbo.Observaciones");
        }
    }
}
