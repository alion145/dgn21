namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SolicitudesBusqueda : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SolicitudesBusqueda",
                c => new
                    {
                        IdSolicitudBusqueda = c.Int(nullable: false, identity: true),
                        IdSolicitud = c.Int(nullable: false),
                        VisualizacionInterna = c.String(),
                        FechaCreacion = c.DateTime(nullable: false, defaultValueSql: "GetDate()"),
                        IdUserCreacion = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.IdSolicitudBusqueda)
                .ForeignKey("dbo.AspNetUsers", t => t.IdUserCreacion)
                .ForeignKey("dbo.Solicitudes", t => t.IdSolicitud, cascadeDelete: true)
                .Index(t => t.IdSolicitud)
                .Index(t => t.IdUserCreacion);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SolicitudesBusqueda", "IdSolicitud", "dbo.Solicitudes");
            DropForeignKey("dbo.SolicitudesBusqueda", "IdUserCreacion", "dbo.AspNetUsers");
            DropIndex("dbo.SolicitudesBusqueda", new[] { "IdUserCreacion" });
            DropIndex("dbo.SolicitudesBusqueda", new[] { "IdSolicitud" });
            DropTable("dbo.SolicitudesBusqueda");
        }
    }
}
