namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class formatospapeleta2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Solicitudes", "IdFormatoPapeleta", c => c.Int(nullable: true));
            CreateIndex("dbo.Solicitudes", "IdFormatoPapeleta");
            AddForeignKey("dbo.Solicitudes", "IdFormatoPapeleta", "dbo.FormatosPapeletas", "IdFormatoPapeleta", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Solicitudes", "IdFormatoPapeleta", "dbo.FormatosPapeletas");
            DropIndex("dbo.Solicitudes", new[] { "IdFormatoPapeleta" });
            DropColumn("dbo.Solicitudes", "IdFormatoPapeleta");
        }
    }
}
