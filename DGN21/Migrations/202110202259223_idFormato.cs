namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class idFormato : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Solicitudes", "IdFormatoPapeleta", "dbo.FormatosPapeletas");
            DropIndex("dbo.Solicitudes", new[] { "IdFormatoPapeleta" });
            AlterColumn("dbo.Solicitudes", "IdFormatoPapeleta", c => c.Int());
            CreateIndex("dbo.Solicitudes", "IdFormatoPapeleta");
            AddForeignKey("dbo.Solicitudes", "IdFormatoPapeleta", "dbo.FormatosPapeletas", "IdFormatoPapeleta");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Solicitudes", "IdFormatoPapeleta", "dbo.FormatosPapeletas");
            DropIndex("dbo.Solicitudes", new[] { "IdFormatoPapeleta" });
            AlterColumn("dbo.Solicitudes", "IdFormatoPapeleta", c => c.Int(nullable: false));
            CreateIndex("dbo.Solicitudes", "IdFormatoPapeleta");
            AddForeignKey("dbo.Solicitudes", "IdFormatoPapeleta", "dbo.FormatosPapeletas", "IdFormatoPapeleta", cascadeDelete: true);
        }
    }
}
