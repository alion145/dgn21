namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cantidadstring : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PapeletasPago", "Cantidad", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PapeletasPago", "Cantidad", c => c.Double(nullable: false));
        }
    }
}
