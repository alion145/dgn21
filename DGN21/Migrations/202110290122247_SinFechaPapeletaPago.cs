namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SinFechaPapeletaPago : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PapeletasPago", "Cantidad", c => c.String());
            DropColumn("dbo.PapeletasPago", "Fecha");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PapeletasPago", "Fecha", c => c.DateTime());
            AlterColumn("dbo.PapeletasPago", "Cantidad", c => c.String());
        }
    }
}
