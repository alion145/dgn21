namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemitePapeletaPago : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormatosPapeletas", "Remite", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormatosPapeletas", "Remite");
        }
    }
}
