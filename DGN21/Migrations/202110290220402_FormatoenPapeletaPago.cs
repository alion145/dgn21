namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FormatoenPapeletaPago : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Solicitudes", "IdFormatoPapeleta", "dbo.FormatosPapeletas");
            DropIndex("dbo.Solicitudes", new[] { "IdFormatoPapeleta" });
            AddColumn("dbo.PapeletasPago", "IdFormatoPapeleta", c => c.Int());
            AlterColumn("dbo.PapeletasPago", "Cantidad", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            CreateIndex("dbo.PapeletasPago", "IdFormatoPapeleta");
            AddForeignKey("dbo.PapeletasPago", "IdFormatoPapeleta", "dbo.FormatosPapeletas", "IdFormatoPapeleta");
            DropColumn("dbo.Solicitudes", "IdFormatoPapeleta");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Solicitudes", "IdFormatoPapeleta", c => c.Int());
            DropForeignKey("dbo.PapeletasPago", "IdFormatoPapeleta", "dbo.FormatosPapeletas");
            DropIndex("dbo.PapeletasPago", new[] { "IdFormatoPapeleta" });
            AlterColumn("dbo.PapeletasPago", "Cantidad", c => c.String());
            DropColumn("dbo.PapeletasPago", "IdFormatoPapeleta");
            CreateIndex("dbo.Solicitudes", "IdFormatoPapeleta");
            AddForeignKey("dbo.Solicitudes", "IdFormatoPapeleta", "dbo.FormatosPapeletas", "IdFormatoPapeleta");
        }
    }
}
