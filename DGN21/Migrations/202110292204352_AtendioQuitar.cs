namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AtendioQuitar : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.PapeletasPago", "Atendio");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PapeletasPago", "Atendio", c => c.String(maxLength: 255));
        }
    }
}
