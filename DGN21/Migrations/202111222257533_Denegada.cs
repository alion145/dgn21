namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Denegada : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TurnadosSolicitudes", "Denegada", c => c.Boolean());
            AddColumn("dbo.TurnadosSolicitudes", "ObservacionInterna", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TurnadosSolicitudes", "ObservacionInterna");
            DropColumn("dbo.TurnadosSolicitudes", "Denegada");
        }
    }
}
