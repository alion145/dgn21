namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Denegada2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TurnadosSolicitudes", "Denegada", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TurnadosSolicitudes", "Denegada", c => c.Boolean());
        }
    }
}
