namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Denegada3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TurnadosSolicitudes", "Denegada", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TurnadosSolicitudes", "Denegada", c => c.Int());
        }
    }
}
