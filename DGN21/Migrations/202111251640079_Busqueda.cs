namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Busqueda : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SolicitudesBusqueda", "AnioEscritura", c => c.String());
            AddColumn("dbo.SolicitudesBusqueda", "Otorgante", c => c.String());
            DropColumn("dbo.SolicitudesBusqueda", "VisualizacionInterna");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SolicitudesBusqueda", "VisualizacionInterna", c => c.String());
            DropColumn("dbo.SolicitudesBusqueda", "Otorgante");
            DropColumn("dbo.SolicitudesBusqueda", "AnioEscritura");
        }
    }
}
