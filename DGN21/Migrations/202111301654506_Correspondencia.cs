namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Correspondencia : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Correspondencia",
                c => new
                    {
                        IdCorrespondencia = c.Int(nullable: false, identity: true),
                        NoOficio = c.String(),
                        Destinatario = c.String(),
                        Remitente = c.String(),
                        Dependencia = c.String(),
                        Asunto = c.String(),
                        FechaOficio = c.DateTime(nullable: false),
                        FechaRecibido = c.DateTime(nullable: false),
                        FechaCreacion = c.DateTime(nullable: false, defaultValueSql: "getdate()"),
                        IdUserCreacion = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.IdCorrespondencia)
                .ForeignKey("dbo.AspNetUsers", t => t.IdUserCreacion)
                .Index(t => t.IdUserCreacion);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Correspondencia", "IdUserCreacion", "dbo.AspNetUsers");
            DropIndex("dbo.Correspondencia", new[] { "IdUserCreacion" });
            DropTable("dbo.Correspondencia");
        }
    }
}
