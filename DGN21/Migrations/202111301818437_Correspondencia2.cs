namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Correspondencia2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Correspondencia", "FechaOficio", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Correspondencia", "FechaRecibido", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Correspondencia", "FechaRecibido", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Correspondencia", "FechaOficio", c => c.DateTime(nullable: false));
        }
    }
}
