// <auto-generated />
namespace DGN21.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class CujusJuzgado : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CujusJuzgado));
        
        string IMigrationMetadata.Id
        {
            get { return "202202032122160_CujusJuzgado"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
