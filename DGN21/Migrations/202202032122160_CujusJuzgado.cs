namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CujusJuzgado : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Solicitudes", "Cujus", c => c.String(maxLength: 100));
            AddColumn("dbo.Solicitudes", "Juzgado", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Solicitudes", "Juzgado");
            DropColumn("dbo.Solicitudes", "Cujus");
        }
    }
}
