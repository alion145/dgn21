namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TerminoCorrespondencia : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Correspondencia", "FechaTermino", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Correspondencia", "FechaTermino");
        }
    }
}
