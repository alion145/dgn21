namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FechasActEstatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Solicitudes", "FechaActualizacionEstatus", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Solicitudes", "FechaActualizacionEstatus");
        }
    }
}
