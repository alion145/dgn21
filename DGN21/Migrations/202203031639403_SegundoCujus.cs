namespace DGN21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SegundoCujus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Solicitudes", "CujusSegundo", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Solicitudes", "CujusSegundo");
        }
    }
}
