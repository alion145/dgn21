﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DGN21.Models
{
    [Table("Correspondencia")]
    public class CorrespondenciaModels
    {
        [Key]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:000000000}")]
        [Display(Name = "Folio")]
        public int IdCorrespondencia { get; set; }

        [Display(Name = "No. de Oficio")]
        public string NoOficio { get; set; }

        [Display(Name = "Destinatario")]
        public string Destinatario { get; set; }

        [Display(Name = "Remitente")]
        public string Remitente { get; set; }

        [Display(Name = "Dependencia")]
        public string Dependencia { get; set; }

        [Display(Name = "Asunto")]
        public string Asunto { get; set; }

        [Display(Name = "Fecha de oficio")]
        [DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Column(TypeName = "datetime2")]
        public DateTime FechaOficio { get; set; }

        [Display(Name = "Fecha de recibido")]
        [Column(TypeName = "datetime2")]
        [DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime FechaRecibido { get; set; }

        [Display(Name = "Fecha de término")]
        [Column(TypeName = "datetime2")]
        [DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime FechaTermino { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? FechaCreacion { get; set; }

        [ForeignKey("ApplicationUser")]
        public string IdUserCreacion { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }

    public class Correspondenciadbcontex : DbContext
    {
        public DbSet<CorrespondenciaModels> Correspondencia { get; set; }
    }
}