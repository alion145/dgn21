﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;

namespace DGN21.Models
{
    [Table("Documentos")]
    public class DocumentosModels
    {
        // PROPIEDADES PÚBLICAS
        [Key]
        public int IdDocumento { get; set; }

        [ForeignKey("Solicitudes")]
        [Display(Name = "Folio")]
        public int IdSolicitud { get; set; }

        [Display(Name = "Solicitud")]
        public virtual SolicitudesModels Solicitudes { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? FechaCreacion { get; set; }

        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }

        [StringLength(4)]
        [Required]
        public string Extension { get; set; }

        // PROPIEDADES PRIVADAS
        public string PathRelativo
        {
            get
            {
                return ConfigurationManager.AppSettings["PathDocumentos"] +
                                            this.IdDocumento.ToString() + "." +
                                            this.Extension;
            }
        }

        public string PathCompleto
        {
            get
            {
                var _PathAplicacion = HttpContext.Current.Request.PhysicalApplicationPath;
                return Path.Combine(_PathAplicacion, this.PathRelativo);
            }
        }

        // MÉTODOS PÚBLICOS
        public void SubirArchivo(byte[] archivo)
        {
            File.WriteAllBytes(this.PathCompleto, archivo);
        }

        public byte[] DescargarArchivo()
        {
            return File.ReadAllBytes(this.PathCompleto);
        }

        public void EliminarArchivo()
        {
            File.Delete(this.PathCompleto);
        }
    }

    public class Documentosdbcontex : DbContext
    {
        public DbSet<DocumentosModels> Documentos { get; set; }
    }
}