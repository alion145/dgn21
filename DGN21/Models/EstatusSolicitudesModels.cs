﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DGN21.Models
{
    [Table("EstatusSolicitudes")]
    public partial class EstatusSolicitudesModels
    {
        [Key]
        public int IdEstatusSolicitud { get; set; }

        [StringLength(50)]
        public string EstatusSolicitud { get; set; }

        public bool Activo { get; set; }
    }

    public class EstatusSolicitudesdbcontex : DbContext
    {
        public DbSet<EstatusSolicitudesModels> EstatusSolicitudes { get; set; }
    }
}