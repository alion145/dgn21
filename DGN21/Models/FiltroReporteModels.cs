﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DGN21.Models
{
    public class FiltroReporteModels
    {
        [Display(Name = "Inicio")]
        public DateTime? FechaInicioReporte { get; set; }

        [Display(Name = "Fin")]
        public DateTime? FechaFinReporte { get; set; }

        [Display(Name = "Estatus")]
        public int EstatusSolicitud { get; set; }

        [Display(Name = "Usuario")]
        public string IdUser { get; set; }
    }
}