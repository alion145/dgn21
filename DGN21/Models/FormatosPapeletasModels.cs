﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DGN21.Models
{
    [Table("FormatosPapeletas")]
    public class FormatosPapeletasModels
    {
        [Key]
        public int IdFormatoPapeleta { get; set; }

        [ForeignKey("TiposTramites")]
        [Required(ErrorMessage = "Tipo de trámite obligatorio.")]
        [Display(Name = "Tipo de trámite")]
        public int IdTipoTramite { get; set; }

        [Display(Name = "Tipo de trámite")]
        public virtual TiposTramitesModels TiposTramites { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Nombre de formato de papeleta obligatorio.")]
        [Display(Name = "Nombre del formato")]
        public string NombreFormato { get; set; }
                
        [Required(ErrorMessage = "Concepto del formato de papeleta obligatorio.")]
        public string Concepto { get; set; }

        [Display(Name = "¿Remite FUERA o DENTRO de Hermosilo Sonora?")]
        public string Remite { get; set; }

        [Required(ErrorMessage = "Clave fiscal obligatoria.")]
        [Display(Name = "Clave fiscal")]
        public long CveFiscal { get; set; }

        public bool Activo { get; set; }
    }

    public class FormatosPapeletasdbcontex : DbContext
    {
        public DbSet<FormatosPapeletasModels> FormatosPapeletas { get; set; }
    }
}