﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DGN21.Models
{
    // Para agregar datos de perfil del usuario, agregue más propiedades a su clase ApplicationUser. Visite https://go.microsoft.com/fwlink/?LinkID=317594 para obtener más información.
    public class ApplicationUser : IdentityUser
    {
        public virtual string Name { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Agregar aquí notificaciones personalizadas de usuario
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<TiposTramitesModels> TiposTramites { get; set; }

        public DbSet<EstatusSolicitudesModels> EstatusSolicitudes { get; set; }

        public DbSet<SolicitudesModels> Solicitudes { get; set; }

        public DbSet<DocumentosModels> Documentos { get; set; }

        public DbSet<TurnadosModels> Turnados { get; set; }

        public DbSet<FormatosPapeletasModels> FormatosPapeletas { get; set; }

        public DbSet<PapeletasPagoModels> PapeletasPago { get; set; }

        public DbSet<ObservacionesSolicitudesModels> ObservacionesSolicitudes { get; set; }

        public DbSet<SolicitudesBusquedaModels> SolicitudesBusqueda { get; set; }

        public DbSet<CorrespondenciaModels> Correspondencia { get; set; }
    }
}