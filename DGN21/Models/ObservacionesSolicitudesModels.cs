﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DGN21.Models
{
    [Table("Observaciones")]
    public class ObservacionesSolicitudesModels
    {
        [Key]
        [Display(Name = "Observaciones")]
        public int IdObservaciones { get; set; }

        [Required]
        [ForeignKey("Solicitudes")]
        [Display(Name = "Folio")]
        public int IdSolicitud { get; set; }

        [Display(Name = "Solicitud")]
        public virtual SolicitudesModels Solicitudes { get; set; }

        [Display(Name = "Observación")]
        public string Observacion { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Fecha")]
        public DateTime? FechaCreacion { get; set; }

        [ForeignKey("ApplicationUser")]
        public string IdUserCreacion { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }

    public class ObservacionesSolicitudesdbcontex : DbContext
    {
        public DbSet<ObservacionesSolicitudesModels> ObservacionesSolicitudes { get; set; }
    }
}