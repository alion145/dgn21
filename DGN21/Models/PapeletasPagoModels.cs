﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DGN21.Models
{
    [Table("PapeletasPago")]
    public class PapeletasPagoModels
    {
        [Key]
        public int IdPapeletaPago { get; set; }

        [Required]
        [ForeignKey("Solicitudes")]
        [Display(Name = "Folio")]
        public int IdSolicitud { get; set; }

        [Display(Name = "Solicitud")]
        public virtual SolicitudesModels Solicitudes { get; set; }

        [ForeignKey("FormatosPapeletas")]
        [Display(Name = "Formato")]
        public int? IdFormatoPapeleta { get; set; }

        [Display(Name = "Formato de papeleta de pago")]
        public virtual FormatosPapeletasModels FormatosPapeletas { get; set; }

        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        public decimal Cantidad { get; set; }

        [StringLength(255)]
        [Display(Name = "Cantidad en letra")]
        public string CantidadLetra { get; set; }
    }

    public class PapeletasPagodbcontex : DbContext
    {
        public DbSet<PapeletasPagoModels> PapeletasPago { get; set; }
    }
}