﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DGN21.Models
{
    [Table("SolicitudesBusqueda")]
    public class SolicitudesBusquedaModels
    {
        [Key]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:000000000}")]
        [Display(Name = "Folio Búsqueda")]
        public int IdSolicitudBusqueda { get; set; }

        [Required]
        [ForeignKey("Solicitudes")]
        [Display(Name = "Folio")]
        public int IdSolicitud { get; set; }

        [Display(Name = "Solicitud")]
        public virtual SolicitudesModels Solicitudes { get; set; }

        [Display(Name = "Año de escritura")]
        public string AnioEscritura { get; set; }

        [Display(Name = "Otorgante")]
        public string Otorgante { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? FechaCreacion { get; set; }

        [ForeignKey("ApplicationUser")]
        public string IdUserCreacion { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }

    public class SolicitudesBusquedadbcontex : DbContext
    {
        public DbSet<SolicitudesBusquedaModels> SolicitudesBusqueda { get; set; }
    }
}