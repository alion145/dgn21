﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DGN21.Models
{
    [Table("Solicitudes")]
    public partial class SolicitudesModels
    {
        public SolicitudesModels()
        {
            this.Documentos = new List<DocumentosModels>();
        }

        [Key]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:000000000}")]
        [Display(Name = "Folio")]
        public int IdSolicitud { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Nombre(s) obligatorio.")]
        [Display(Name = "Nombre(s)")]
        public string NombreSolicitante { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Apellido paterno obligatorio.")]
        [Display(Name = "Apellido Paterno")]
        public string ApellidoPaterno { get; set; }

        [StringLength(100)]
        [Display(Name = "Apellido Materno")]
        public string ApellidoMaterno { get; set; }

        [StringLength(255)]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Correo electrónico no válido.")]
        [Required(ErrorMessage = "Correo electrónico obligatorio.")]
        [Display(Name = "Correo Electrónico (E-mail de seguimiento)")]
        public string CorreoElectronico { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "Teléfono obligatorio.")]
        [Display(Name = "Teléfono")]
        public string Telefono { get; set; }

        [StringLength(50)]
        public string Escritura { get; set; }

        [StringLength(50)]
        [Display(Name = "Vol.")]
        public string Vol { get; set; }

        [StringLength(50)]
        [Display(Name = "No. Notaria")]
        public string NoNotaria { get; set; }

        [StringLength(50)]
        public string Notario { get; set; }

        [StringLength(50)]
        [Display(Name = "Demarcación")]
        public string Demarcacion { get; set; }
        
        [Display(Name = "Comparece escritura")]
        public bool CompareceEscritura { get; set; }

        [ForeignKey("TiposTramites")]
        [Display(Name = "Tipo de trámite")]
        public int TipoTramite { get; set; }

        [Display(Name = "Tipo de trámite")]
        public virtual TiposTramitesModels TiposTramites { get; set; }

        [StringLength(100)]
        [Display(Name = "Nombre de cujus (finado)")]
        public string Cujus { get; set; }

        [StringLength(100)]
        [Display(Name = "Nombre de segundo cujus (finado)")]
        public string CujusSegundo { get; set; }

        [StringLength(100)]
        [Display(Name = "Juzgado ó notaría que lo solicita")]
        public string Juzgado { get; set; }

        [ForeignKey("EstatusSolicitudes")]
        [Display(Name = "Estatus")]
        public int EstatusSolicitud { get; set; }

        [Display(Name = "Estatus")]
        public virtual EstatusSolicitudesModels EstatusSolicitudes { get; set; }

        [Display(Name = "Fecha actualización de estatus")]
        [Column(TypeName = "datetime2")]
        [DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime FechaActualizacionEstatus { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? FechaCreacion { get; set; }

        [Display(Name = "Documentos")]
        public virtual List<DocumentosModels> Documentos { get; set; }

        [ForeignKey("ApplicationUser")]
        public string IdUserCreacion { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }

    public class Solicitudesdbcontex : DbContext
    {
        public DbSet<SolicitudesModels> Solicitudes { get; set; }
    }
}