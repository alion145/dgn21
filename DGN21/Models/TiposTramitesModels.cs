﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DGN21.Models
{
    [Table("TiposTramites")]
    public partial class TiposTramitesModels
    {
        [Key]
        public int IdTipoTramite { get; set; }

        [StringLength(50)]
        public string TipoTramite { get; set; }

        public bool Activo { get; set; }
    }

    public class TiposTramitesdbcontex : DbContext
    {
        public DbSet<TiposTramitesModels> TiposTramites { get; set; }
    }
}