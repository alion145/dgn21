﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DGN21.Models
{
    [Table("TurnadosSolicitudes")]
    public class TurnadosModels
    {
        [Key]
        public int IdTurnado { get; set; }

        [ForeignKey("Solicitudes")]
        [Display(Name = "Folio")]
        public int IdSolicitud { get; set; }

        [Display(Name = "Solicitud")]
        public virtual SolicitudesModels Solicitudes { get; set; }

        [ForeignKey("ApplicationUserTurnado")]
        public string IdUserTurnado { get; set; }

        public virtual ApplicationUser ApplicationUserTurnado { get; set; }

        [ForeignKey("ApplicationUser")]
        public string IdUserCreacion { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? FechaCreacion { get; set; }

        [Display(Name = "Denegada interna")]
        public bool Denegada { get; set; }

        [Display(Name = "Observación interna")]
        public string ObservacionInterna { get; set; }
    }

    public class Turnadosdbcontex : DbContext
    {
        public DbSet<TurnadosModels> Turnados { get; set; }
    }
}