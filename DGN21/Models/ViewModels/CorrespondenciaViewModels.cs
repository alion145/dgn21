﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DGN21.Models.ViewModels
{
    public class CorrespondenciaViewModels
    {
        public CorrespondenciaViewModels()
        {
        }

        public CorrespondenciaModels Correspondencia { get; set; }

        public FiltroReporteModels FiltroReporte { get; set; }

        public List<CorrespondenciaModels> CorrespondenciaInbox { get; set; }
    }
}