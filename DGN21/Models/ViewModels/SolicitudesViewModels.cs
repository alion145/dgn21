﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DGN21.Models.ViewModels
{
    public class SolicitudesViewModels
    {
        public SolicitudesViewModels()
        {
        }

        public SolicitudesModels Solicitud { get; set; }

        public SolicitudesBusquedaModels SolicitudBusqueda { get; set; }

        public TurnadosModels Turnada { get; set; }

        [Display(Name = "Papeleta de pago")]
        public PapeletasPagoModels PapeletasPago { get; set; }

        public ObservacionesSolicitudesModels Observacion { get; set; }

        public List<SolicitudesModels> Solicitudes { get; set; }

        public List<ObservacionesSolicitudesModels> Observaciones { get; set; }

        public List<SolicitudInbox> SolicitudesInbox = new List<SolicitudInbox>();

        public FiltroReporteModels FiltroReporte = new FiltroReporteModels();

        public class SolicitudInbox
        {            
            public SolicitudesModels Solicitud { get; set; }

            [Display(Name = "Papeleta de pago")]
            public PapeletasPagoModels PapeletasPago { get; set; }

            public ObservacionesSolicitudesModels Observacion { get; set; }

            public TurnadosModels Turnada { get; set; }

            public List<ObservacionesSolicitudesModels> ObservacionesUltimoTurnado { get; set; }
        }
    }
}