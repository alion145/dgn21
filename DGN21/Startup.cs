﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DGN21.Startup))]
namespace DGN21
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
